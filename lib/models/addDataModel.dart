import 'package:ashvin_jadav/main.dart';
import 'package:objectbox/objectbox.dart';

@Entity()
class AddDataModel {
  String? name;
  String? rold;
  String? fromDate;
  String? toDate;
  int? id;
  AddDataModel({this.name, this.rold, this.fromDate, this.toDate,this.id});
}
class AddDataModelHelper {
  late Box<AddDataModel> addEntryBox;

  init() async {
    addEntryBox = store.box<AddDataModel>();
  }

  Future<int> insert(AddDataModel addEntryModel) async {
    return addEntryBox.put(
      addEntryModel,
      mode: PutMode.insert,
    );
  }

  Future<int> update(AddDataModel addEntryModel) async {
    return addEntryBox.put(addEntryModel, mode: PutMode.put);
  }

  Future<bool> delete(int id) async {
    return addEntryBox.remove(id);
  }

  Future<List<AddDataModel>> getAll() async {
    //return addEntryBox.getAll().reversed.toList();
    return addEntryBox.getAll();
  }

  Future<AddDataModel?> get(int id) async {
    return addEntryBox.get(id);
  }
}