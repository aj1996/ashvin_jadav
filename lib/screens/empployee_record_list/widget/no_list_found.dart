import 'package:ashvin_jadav/utils/ashvin_text.dart';
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:ashvin_jadav/utils/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';

class NoListFound extends StatelessWidget {
  const NoListFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Image.asset(noData,scale: 2),
          NormalText(
            'No employee records found',
            fontSize: common16Size,
            color: AshvinColor.kTitle,
            fontWeight: FontWeight.w500,
          ),
        ],
      ),
    );
  }
}
