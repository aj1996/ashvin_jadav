import 'package:ashvin_jadav/controller/employeeRecordController.dart';
import 'package:ashvin_jadav/models/addDataModel.dart';
import 'package:ashvin_jadav/screens/add_employee_details/screen/add_employee_details.dart';
import 'package:ashvin_jadav/screens/empployee_record_list/widget/no_list_found.dart';
import 'package:ashvin_jadav/utils/ashvin_text.dart';
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:ashvin_jadav/utils/container.dart';
import 'package:ashvin_jadav/utils/extras.dart';
import 'package:ashvin_jadav/utils/images.dart';
import 'package:ashvin_jadav/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';

class EmployeeRecordList extends StatefulWidget {
  EmployeeRecordList({Key? key}) : super(key: key);

  @override
  State<EmployeeRecordList> createState() => _EmployeeRecordListState();
}

class _EmployeeRecordListState extends State<EmployeeRecordList> {
  EmployeeRecordController controller = Get.put(EmployeeRecordController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AshvinColor.kWhite,
      appBar: AppBar(
        title: NormalText(
          'Employee List',
          color: AshvinColor.kWhite,
        ),
        backgroundColor: AshvinColor.kPrimary,
      ),
      body: /* NoListFound()*/
          GetBuilder<EmployeeRecordController>(builder: (controller) {
        return Column(
          children: [
            Expanded(
              child: FutureBuilder<List<AddDataModel>>(
                future: controller.getAllData(),
                builder: (BuildContext context, snapShot) {
                  if (snapShot.hasData && snapShot.data != null) {
                    if (snapShot.data!.isEmpty) {
                      return Center(child: loadSvg(image: NO_DATA_SVG));
                    }
                    List<AddDataModel> addDataModel = snapShot.data!;
                    addDataModel.insert(0, AddDataModel(id: 111111));
                    if (snapShot.data!.length > 3) {
                      addDataModel.insert(4, AddDataModel(id: 222222));
                    }
                    return Column(
                      children: [
                        Expanded(
                            child: ListView.builder(
                                itemCount: addDataModel.length,
                                physics: NeverScrollableScrollPhysics(),
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: [
                                      if (addDataModel[index].id == 111111 ||
                                          addDataModel[index].id == 222222)
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Container(
                                                height: 50,
                                                color: Color(0xffF2F2F2),
                                                padding: EdgeInsets.only(
                                                    top: 10, left: 15),
                                                child: NormalText(
                                                    addDataModel[index].id ==
                                                            111111
                                                        ? 'Current employee'
                                                        : "Previous employees",
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w500,
                                                    color:
                                                        AshvinColor.kPrimary),
                                              ),
                                            ),
                                          ],
                                        )else
                                      Slidable(
                                        endActionPane: ActionPane(
                                          motion: const ScrollMotion(),
                                          children: [
                                            SlidableAction(
                                              onPressed: (a) {
                                                controller.deleteData(
                                                    context: context,
                                                    id: addDataModel[index].id!);
                                              },
                                              backgroundColor:
                                                  Color(0xFFFE4A49),
                                              foregroundColor: Colors.white,
                                              icon: Icons.delete,
                                            ),
                                          ],
                                        ),
                                        child: ListTile(
                                          onTap: () {
                                            Get.to(AddEmployeeDetails(
                                              addDataModel:
                                              addDataModel[index],
                                            ))!
                                                .then((value) {
                                              setState(() {});
                                            });
                                          },
                                          title: NormalText(
                                            '${addDataModel[index].name}',
                                            fontSize: common17Size,
                                            color: AshvinColor.kTitle,
                                            fontWeight: FontWeight.w500,
                                          ),
                                          subtitle: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              NormalText(
                                                '${addDataModel[index].rold}',
                                                fontSize: commonTextSize,
                                                color: AshvinColor.kSubTitle,
                                              ).paddingSymmetric(vertical: 8),
                                              NormalText(
                                                (addDataModel[index].toDate!
                                                            .isEmpty
                                                        ? 'From '
                                                        : '') +
                                                    '${addDataModel[index].fromDate}' +
                                                    (addDataModel[index]
                                                            .toDate!.isNotEmpty
                                                        ? ' - ${addDataModel[index].toDate}'
                                                        : ''),
                                                fontSize: commonTextSize,
                                                color: AshvinColor.kSubTitle,
                                              ),
                                            ],
                                          ),
                                        ).paddingOnly(bottom: 15),
                                      ),
                                    ],
                                  );
                                })),
                        Container(
                          height: 95,
                          color: Color(0xffF2F2F2),
                          padding: EdgeInsets.all(10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              NormalText(
                                SWIPE_LEFT_TO_DELETE,
                                fontSize: commonTextSize,
                                color: AshvinColor.kSubTitle,
                              ),
                            ],
                          ),
                        )
                      ],
                    );
                  }
                  return Center(child: loadSvg(image: NO_DATA_SVG));
                },
              ),
            ),
          ],
        );
      }),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: InkWell(
        onTap: () {
          Get.to(() => AddEmployeeDetails())!.then((value) {
            setState(() {});
          });
        },
        child: myButton(
            child: Center(
                child: Icon(
              Icons.add,
              color: AshvinColor.kWhite,
            )),
            height: 50,
            width: 50),
      ),
    );
  }
}
