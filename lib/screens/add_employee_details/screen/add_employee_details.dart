import 'package:ashvin_jadav/common/app_constant.dart';
import 'package:ashvin_jadav/controller/addEmployeeContoller.dart';
import 'package:ashvin_jadav/models/addDataModel.dart';
import 'package:ashvin_jadav/utils/ashvin_text.dart';
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:ashvin_jadav/utils/container.dart';
import 'package:ashvin_jadav/utils/datePicker/myDatePicker.dart';
import 'package:ashvin_jadav/utils/decorations.dart';
import 'package:ashvin_jadav/utils/extras.dart';
import 'package:ashvin_jadav/utils/images.dart';
import 'package:ashvin_jadav/utils/strings.dart';
import 'package:ashvin_jadav/utils/textField.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class AddEmployeeDetails extends StatefulWidget {
  final AddDataModel? addDataModel;

  const AddEmployeeDetails({Key? key, this.addDataModel}) : super(key: key);

  @override
  State<AddEmployeeDetails> createState() => _AddEmployeeDetailsState();
}

class _AddEmployeeDetailsState extends State<AddEmployeeDetails> {
  AddEmployeeController controller = Get.put(AddEmployeeController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.addDataModel != null) {
      controller.setData(addDataModel: widget.addDataModel!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        title: NormalText(
          widget.addDataModel != null ? EDIT_EMPLOYEE : ADD_EMPLOYEE,
          color: AshvinColor.kWhite,
        ),
        actions: [
          if (widget.addDataModel != null)
            IconButton(onPressed: () {controller.delete(widget.addDataModel!.id!,context);}, icon: loadSvg(image: DELETE_SVG))
        ],
        backgroundColor: AshvinColor.kPrimary,
      ),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 25.0, horizontal: 10),
              child: Column(
                children: [
                  myTextField(
                      controller: controller.txtName,
                      hintText: EMPLOYEE_NAME,
                      prefix: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: loadSvg(
                          image: NAME_SVG,
                        ),
                      )),
                  spaceHeight(20),
                  myTextField(
                      controller: controller.txtRole,
                      hintText: SELECT_ROLE,
                      readOnly: true,
                      prefix: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: loadSvg(image: ROLE_SVG),
                      ),
                      onTap: () {
                        controller.getRole();
                      },
                      suffix: Icon(
                        Icons.arrow_drop_down,
                        size: 35,
                        color: AshvinColor.kPrimary,
                      )),
                  spaceHeight(20),
                  Row(
                    children: [
                      Expanded(
                        child: myTextField(
                            controller: controller.txtFromDate,
                            hintText: TODAY,
                            hintBlack: true,
                            onTap: () async {
                              controller.getFromDate(context: context);
                            },
                            readOnly: true,
                            prefix: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: loadSvg(image: CALENDAR_SVG),
                            )),
                      ),
                      spaceWidth(20),
                      loadSvg(
                        image: ARROW_SVG,
                      ),
                      spaceWidth(20),
                      Expanded(
                        child: myTextField(
                            controller: controller.txtToDate,
                            hintText: NO_DATE,
                            readOnly: true,
                            onTap: () {
                              controller.getToDate(context: context);
                            },
                            prefix: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: loadSvg(image: CALENDAR_SVG),
                            )),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 15.0, right: 15, top: 5, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: myButton(
                      height: 40,
                      width: 75,
                      cornerRadius: 5,
                      color: AshvinColor.kButtonDeactivate,
                      child: Center(
                        child: NormalText(
                          CANCEL,
                          fontWeight: FontWeight.w500,
                          fontSize: 14,
                          color: AshvinColor.kPrimary,
                        ),
                      )),
                ),
                spaceWidth(10),
                InkWell(
                  onTap: () {
                    controller.save(addDataModel: widget.addDataModel);
                  },
                  child: myButton(
                      height: 40,
                      width: 75,
                      cornerRadius: 5,
                      color: AshvinColor.kPrimary,
                      child: Center(
                        child: NormalText(
                          SAVE,
                          fontWeight: FontWeight.w500,
                          fontSize: 14,
                          color: AshvinColor.kWhite,
                        ),
                      )),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
