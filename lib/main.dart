import 'package:ashvin_jadav/common/app_constant.dart';
import 'package:ashvin_jadav/database/helperClasses.dart';
import 'package:ashvin_jadav/database/objectbox.g.dart';
import 'package:ashvin_jadav/models/addDataModel.dart';
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'screens/empployee_record_list/screen/employee_record_screen.dart';
late final Store store;

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  store=await openStore();
  await addDataModelHelper.init();
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      defaultTransition: Transition.fadeIn,
      builder: (context, child) => ResponsiveWrapper.builder(child,
          maxWidth: 1200,
          minWidth: 480,
          defaultScale: true,
          breakpoints: const [
            ResponsiveBreakpoint.resize(480, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: TABLET),
            ResponsiveBreakpoint.resize(1000, name: DESKTOP),
          ],
          background: Container(color: const Color(0xFFF5F5F5))),
      initialRoute: "/",
      title: "Ashvin jadav",
      theme: ThemeData(
        fontFamily: getFont(),
        primarySwatch: swatchColor,
        primaryColorLight: AshvinColor.kPrimary,
        primaryColor: AshvinColor.kPrimary,
      ),
      home:  EmployeeRecordList(),
    );
  }
}
