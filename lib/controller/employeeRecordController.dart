import 'package:ashvin_jadav/database/helperClasses.dart';
import 'package:ashvin_jadav/models/addDataModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EmployeeRecordController extends GetxController {
  Future<List<AddDataModel>> getAllData() {
    return addDataModelHelper.getAll();
  }

  deleteData({required int id, required BuildContext context}) async {
    AddDataModel? addDataModel = await addDataModelHelper.get(id);
    addDataModelHelper.delete(id);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: Duration(seconds: 2),
        content: Text("Employee data has been deleted"),
        action: SnackBarAction(
          label: "Undo",
          onPressed: () {
            addDataModelHelper.insert(addDataModel!);
            update();
          },
        ),
      ),
    );
    update();
  }
}
