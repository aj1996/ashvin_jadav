import 'package:ashvin_jadav/common/app_constant.dart';
import 'package:ashvin_jadav/database/helperClasses.dart';
import 'package:ashvin_jadav/models/addDataModel.dart';
import 'package:ashvin_jadav/utils/ashvin_text.dart';
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:ashvin_jadav/utils/decorations.dart';
import 'package:ashvin_jadav/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../utils/datePicker/myDatePicker.dart';

class AddEmployeeController extends GetxController {
  TextEditingController txtName = TextEditingController(),
      txtRole = TextEditingController(),
      txtFromDate = TextEditingController(),
      txtToDate = TextEditingController();

  void getFromDate({required BuildContext context}) async {
    String? result = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15))),
          contentPadding: EdgeInsets.zero,
          content: MyDatePicker(
            isFromDate: true,
          ),
        );
      },
    );
    print("Result from date is ${result}");
    if (result != null) {
      txtFromDate.text = result;
    }
  }

  String getDateTime(DateTime dateTime) {
    String formattedDate = DateFormat('dd MMM yyyy').format(dateTime);
    //toDate.value = formattedDate;
    return formattedDate;
  }

  void getToDate({required BuildContext context}) async {
    String? result = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15))),
          contentPadding: EdgeInsets.zero,
          content: MyDatePicker(
            isFromDate: false,
          ),
        );
      },
    );
    print("Result from toDate is ${result}");
    if (result != null) {
      txtToDate.text = result;
    }
  }

  bool validate() {
    if (txtName.text.isEmpty) {
      Fluttertoast.showToast(msg: "Please enter employee name");
      return false;
    }
    if (txtRole.text.isEmpty) {
      Fluttertoast.showToast(msg: "Please select role");
      return false;
    }
    return true;
  }

  save({required AddDataModel? addDataModel}) async {
    if (validate()) {
      print(
          "All ok ${txtName.text} ${txtRole.text} ${txtFromDate.text} ${txtToDate.text}");
      if(addDataModel!=null){
        addDataModel.name=txtName.text;
        addDataModel.rold=txtRole.text;
        addDataModel.fromDate=txtFromDate.text;
        addDataModel.toDate=txtToDate.text;
        int id = await addDataModelHelper.update(addDataModel);
        print("Data updated and id is ${id}");
      }else{
        AddDataModel addDataModel = AddDataModel(
            name: txtName.text,
            rold: txtRole.text,
            fromDate: txtFromDate.text == TODAY || txtFromDate.text.isEmpty
                ? '${getDateTime(DateTime.now())}'
                : txtFromDate.text,
            toDate: txtToDate.text);
        int id = await addDataModelHelper.insert(addDataModel);
        print("Data added and id is ${id} and ${addDataModel.fromDate}");
      }
      Get.back();
    }
  }

  void getRole() async {
    String? role = await Get.bottomSheet(Container(
      height: Get.height * 0.30,
      decoration: roundCornerDecoration(
          color: Colors.white, circularSize: 10, corner: 0),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Column(
          children: List.generate(
              ROLE_LIST.length,
              (index) => InkWell(
                    onTap: () {
                      Get.back(result: ROLE_LIST[index]);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              NormalText(
                                ROLE_LIST[index],
                                fontSize: common16Size,
                                color: AshvinColor.kTitle,
                                fontWeight: FontWeight.w400,
                              ),
                            ],
                          ),
                          Divider(
                            color: Color(0xffF2F2F2),
                          ),
                        ],
                      ),
                    ),
                  )),
        ),
      ),
    ));
    print("Role is $role");
    if (role != null) {
      txtRole.text = role;
    }
  }

  void setData({required AddDataModel addDataModel}) {
    txtName.text=addDataModel.name!;
    txtRole.text=addDataModel.rold!;
    txtFromDate.text=addDataModel.fromDate!;
    txtToDate.text=addDataModel.toDate!;
  }

  void delete(int id,BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text("Are you sure you want to delete this item?"),
          actions: <Widget>[
            TextButton(
              child: Text("No"),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            TextButton(
              child: Text("Yes"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    ).then((confirmed) async{
      if (confirmed != null && confirmed) {
        await addDataModelHelper.delete(id);
        Get.back();
      }
    });
  }
}
