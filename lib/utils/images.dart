

/// List screen
const String noData = "assets/images/no_data.png";
const String NO_DATA_SVG = "assets/svg/nodata.svg";
const String ARROW_SVG = "assets/svg/arrow.svg";
const String CALENDAR_SVG = "assets/svg/calendar.svg";
const String DELETE_SVG = "assets/svg/delete.svg";
const String NAME_SVG = "assets/svg/name.svg";
const String ROLE_SVG = "assets/svg/role.svg";
