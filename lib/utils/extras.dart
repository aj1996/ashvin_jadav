import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

Widget loadSvg(
    {required String image, double? height, double? width, Color? color}) {
  return SvgPicture.asset(image, height: height, width: width, color: color);
}