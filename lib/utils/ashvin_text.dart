
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:ashvin_jadav/utils/reedius_text_style.dart';
import 'package:flutter/material.dart';

 double commonBoxHeight = 50;
 double commonHintSize = 16;
 double common16Size = 16;
 double common17Size = 17;
 double common19Size = 19;
 double common18Size = 18;
 double common24Size = 24;
 double common20Size = 20;
 double common22Size = 22;
 double common30Size = 30;
 double common28Size = 28;
 double commonCorner = 14;

 double commonTextSize = 15;
 double common14Size = 14;
 double common13Size = 13;
 double common11Size = 11;
 double common10Size = 10;
 double common12Size = 12;
 double headlineTextSize = 17;

class NormalText extends Text {
  NormalText(String data,
      {Key? key,
        FontWeight? fontWeight=FontWeight.w400,
        double fontSize = 15,
        Color color = AshvinColor.kBlack,
        TextAlign? textAlign,
        TextDecoration? textDecoration,
        TextOverflow? overflow,
        int? maxLines})
      : super(data,
      key: key,
      textAlign: textAlign,
      overflow: overflow,
      maxLines: maxLines,

      style: normalStyle(
          fontWeight: fontWeight,
          fontSize: fontSize,
          color: color,
          decoration: textDecoration));
}

class NormalBoldText extends Text {
      NormalBoldText(String data,
      {Key? key,
        FontWeight? fontWeight=FontWeight.w500,
        double fontSize = 15,
        Color color = AshvinColor.titleColor3,
        TextAlign? textAlign,
        TextDecoration? textDecoration,
        TextOverflow? overflow,
        int? maxLines})
      : super(data,
      key: key,
      textAlign: textAlign,
      overflow: overflow,
      maxLines: maxLines,

      style: normalStyle(
          fontWeight: fontWeight,
          fontSize: fontSize,
          color: color,
          decoration: textDecoration));
}



