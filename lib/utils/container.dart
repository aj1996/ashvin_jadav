import 'package:ashvin_jadav/utils/colors.dart';
import 'package:flutter/material.dart';

Widget myButton(
    {double? height,
    double? width,
    double cornerRadius = 10,
    Color color = AshvinColor.kPrimary,
    required Widget child}) {
  return Container(
    height: height,
    width: width,
    decoration: BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(cornerRadius),
    ),
    child: child,
  );
}

spaceHeight(double height) {
  return SizedBox(
    height: height,
  );
}
spaceWidth(double width) {
  return SizedBox(
    width: width,
  );
}
