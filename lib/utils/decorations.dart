
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:flutter/material.dart';

BoxDecoration customBoxDecoration(
    {Color? color = AshvinColor.kWhite, double? round = 10,Gradient? gradient}) {
  return BoxDecoration(
      color: color,gradient: gradient,
      borderRadius: BorderRadius.all(
        Radius.circular(round!),

      ));
}

BoxDecoration roundCornerDecoration(
    {required Color color,
      Color? bgColor = Colors.white,
      required double circularSize,
      required double corner}) {
  return BoxDecoration(
      color: bgColor,
      border: Border.all(color: color, width: corner),
      borderRadius: BorderRadius.all(
        Radius.circular(circularSize),
      ));
}