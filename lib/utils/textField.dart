import 'package:ashvin_jadav/utils/ashvin_text.dart';
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:flutter/material.dart';

Widget myTextField(
    {required TextEditingController controller,
    required String hintText,
    Widget? prefix,
      bool hintBlack=false,
    bool readOnly = false,
      Function()? onTap,
    Widget? suffix}) {
  return Container(
    height: 42,
    child: TextField(
      readOnly: readOnly,
      controller: controller,
      onTap: onTap,
      style: TextStyle(
        fontSize: common17Size,
        color: AshvinColor.kTitle,
        fontWeight: FontWeight.w400,
      ),
      decoration: InputDecoration(
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          prefixIcon: prefix,
          hintText: hintText,
          suffixIcon: suffix,
          hintStyle: TextStyle(
            fontSize: common17Size,
            color: hintBlack?AshvinColor.kTitle: AshvinColor.kSubTitle,
            fontWeight: FontWeight.w400,
          ),
          contentPadding: EdgeInsets.only(top: 10)),
    ),
  );
}
