import 'package:ashvin_jadav/utils/ashvin_text.dart';
import 'package:ashvin_jadav/utils/colors.dart';
import 'package:ashvin_jadav/utils/container.dart';
import 'package:ashvin_jadav/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class MyDatePicker extends StatefulWidget {
  final bool isFromDate;

  const MyDatePicker({Key? key, required this.isFromDate}) : super(key: key);

  @override
  _MyDatePickerState createState() => _MyDatePickerState();
}

class _MyDatePickerState extends State<MyDatePicker> {
  DateRangePickerController _dateRangePickerController =
      DateRangePickerController();
  int activeIndex = 0;
  String selectedDate = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.isFromDate) {
      changeIndex(0);
    } else {
      changeIndexToDate(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      width: Get.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            widget.isFromDate
                ? Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      changeIndex(0);
                                    },
                                    child: Container(
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: activeIndex == 0
                                                ? AshvinColor.kPrimary
                                                : AshvinColor.kButtonDeactivate,
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            NormalText(
                                              TODAY,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: activeIndex == 0
                                                  ? AshvinColor.kWhite
                                                  : AshvinColor.kPrimary,
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          spaceWidth(20),
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      changeIndex(1);
                                    },
                                    child: Container(
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: activeIndex == 1
                                                ? AshvinColor.kPrimary
                                                : AshvinColor.kButtonDeactivate,
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            NormalText(
                                              NEXT_MON,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: activeIndex == 1
                                                  ? AshvinColor.kWhite
                                                  : AshvinColor.kPrimary,
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      spaceHeight(15),
                      Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      changeIndex(2);
                                    },
                                    child: Container(
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: activeIndex == 2
                                                ? AshvinColor.kPrimary
                                                : AshvinColor.kButtonDeactivate,
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            NormalText(
                                              NEXT_THU,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: activeIndex == 2
                                                  ? AshvinColor.kWhite
                                                  : AshvinColor.kPrimary,
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          spaceWidth(20),
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      changeIndex(3);
                                    },
                                    child: Container(
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: activeIndex == 3
                                                ? AshvinColor.kPrimary
                                                : AshvinColor.kButtonDeactivate,
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            NormalText(
                                              AFTER_WEEK,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: activeIndex == 3
                                                  ? AshvinColor.kWhite
                                                  : AshvinColor.kPrimary,
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  )
                : Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      changeIndexToDate(0);
                                    },
                                    child: Container(
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: activeIndex == 0
                                                ? AshvinColor.kPrimary
                                                : AshvinColor.kButtonDeactivate,
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            NormalText(
                                              NO_DATE,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: activeIndex == 0
                                                  ? AshvinColor.kWhite
                                                  : AshvinColor.kPrimary,
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          spaceWidth(20),
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      changeIndexToDate(1);
                                    },
                                    child: Container(
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: activeIndex == 1
                                                ? AshvinColor.kPrimary
                                                : AshvinColor.kButtonDeactivate,
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            NormalText(
                                              TODAY,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: activeIndex == 1
                                                  ? AshvinColor.kWhite
                                                  : AshvinColor.kPrimary,
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
            spaceHeight(15),
            SfDateRangePicker(
              onSelectionChanged: (DateRangePickerSelectionChangedArgs a) {
                print("selectedDate ${a.value}");
                updateDateString(date: '${a.value.toString()}');
              },
              controller: _dateRangePickerController,
              selectionMode: DateRangePickerSelectionMode.single,
              showNavigationArrow: true,
              navigationDirection:
                  DateRangePickerNavigationDirection.horizontal,
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.center,
                  textStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      color: Colors.black)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.calendar_today,
                      color: AshvinColor.kPrimary,
                      size: 25,
                    ),
                    spaceWidth(10),
                    NormalText(
                      '${selectedDate}',
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    )
                  ],
                ),
                Row(
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: myButton(
                          height: 40,
                          width: 75,
                          cornerRadius: 5,
                          color: AshvinColor.kButtonDeactivate,
                          child: Center(
                            child: NormalText(
                              CANCEL,
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: AshvinColor.kPrimary,
                            ),
                          )),
                    ),
                    spaceWidth(10),
                    InkWell(
                      onTap: () {
                        Get.back(result: selectedDate);
                      },
                      child: myButton(
                          height: 40,
                          width: 75,
                          cornerRadius: 5,
                          color: AshvinColor.kPrimary,
                          child: Center(
                            child: NormalText(
                              SAVE,
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: AshvinColor.kWhite,
                            ),
                          )),
                    ),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  void changeIndex(int i) {
    setState(() {
      activeIndex = i;
      switch (i) {
        case 0:
          _dateRangePickerController.selectedDate = DateTime.now();
          selectedDate = TODAY;
          setState(() {});
          break;
        case 1:
          _dateRangePickerController.selectedDate = getNextMonday();
          updateDateString(date: '${getNextMonday().toString()}');

          break;
        case 2:
          _dateRangePickerController.selectedDate = getNextTuesday();
          updateDateString(date: '${getNextTuesday().toString()}');

          break;
        case 3:
          _dateRangePickerController.selectedDate =
              DateTime.now().add(Duration(days: 7));
          updateDateString(
              date: '${DateTime.now().add(Duration(days: 7)).toString()}');

          break;
      }
    });
  }

  updateDateString({required String date}) {
    DateTime dateTime = DateTime.parse(date);
    String formattedDate = DateFormat('dd MMM yyyy').format(dateTime);
    selectedDate = formattedDate;
    setState(() {});
  }

  void changeIndexToDate(int i) {
    setState(() {
      activeIndex = i;
      switch (i) {
        case 0:
          selectedDate = NO_DATE;
          setState(() {});
          break;
        case 1:
          _dateRangePickerController.selectedDate = getNextMonday();
          updateDateString(date: '${getNextMonday().toString()}');
          break;
      }
    });
  }

  DateTime getNextMonday() {
    DateTime now = DateTime.now();
    int daysUntilMonday = DateTime.monday - now.weekday;
    if (daysUntilMonday <= 0) {
      daysUntilMonday += 7;
    }
    return now.add(Duration(days: daysUntilMonday));
  }

  DateTime getNextTuesday() {
    DateTime now = DateTime.now();
    int daysUntilTuesday = DateTime.tuesday - now.weekday;
    if (daysUntilTuesday <= 0) {
      daysUntilTuesday += 7;
    }
    return now.add(Duration(days: daysUntilTuesday));
  }
}
