

import 'package:ashvin_jadav/utils/colors.dart';
import 'package:flutter/material.dart';

const kPrimaryTextSize = 17.0;
const kSecondaryTextSize = 14.0;
const kTertiaryTextSize = 12.0;
const kNormalTextSize = 14.0;
const kMediumTextSize = 14.0;
const kCardRadius = 8.0;
const kButtonRadius = 12.0;
const kHeadingTextSize = 20.0;
const kHeadingSecondaryTextSize = 18.0;
const kAppBarHeight = 50.0;
const kSearchBarRadius = 12.0;

///Primary TextStyle for [PrimaryText]
TextStyle primaryStyle(
    {double fontSize = kPrimaryTextSize, FontWeight? fontWeight}) {
  return TextStyle(
      fontSize: fontSize,
      color: AshvinColor.kPrimaryText,
      fontWeight: fontWeight);
}



///Tertiary TextStyle to smaller text
TextStyle tertiaryStyle(
    {double fontSize = kTertiaryTextSize, FontWeight? fontWeight,Color? color}) {
  return TextStyle(
      fontSize: fontSize,
      color:color,
      fontWeight: fontWeight);
}

///Customizable TextStyle
TextStyle normalStyle(
    {Color color = AshvinColor.kPrimaryText,
    double fontSize = kNormalTextSize,
    FontWeight? fontWeight,
    TextStyle? textStyle,
    TextDecoration? decoration}) {
  return textStyle ??
      TextStyle(
          decoration: decoration,
          fontSize: fontSize,
          height: 1.5,
          //fontFamily: PapasFonts.kFontFam,
          color: color,
          fontWeight: fontWeight);
}///Customizable TextStyle
TextStyle normalBoldStyle(
    {Color color = AshvinColor.kPrimaryText,
    double fontSize = kNormalTextSize,
    FontWeight? fontWeight,
    TextStyle? textStyle,
    TextDecoration? decoration}) {
  return textStyle ??
      TextStyle(
          decoration: decoration,
          fontSize: fontSize,
          height: 1.5,
        //  fontFamily: PapasFonts.kFontFam,
          color: color,
          fontWeight: fontWeight);
}
