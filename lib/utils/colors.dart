import 'package:flutter/material.dart';

class AshvinColor {

  static const kPrimary = Color(0xFF1DA1F2);
  static const kButtonDeactivate = Color(0xFFEDF8FF);
  static const kBlack = Color(0xFF000000);
  static const kWhite = Color(0xFFFFFFFF);
  static const kPrimaryText = Color(0xFF1DA1F2);
  static const Color titleColor3 = Color(0XFF404040);
  static const Color kTitle = Color(0XFF323238);
  static const Color kSubTitle = Color(0XFF949C9E);


}

var swatchColor = const MaterialColor(0xFF3484C6, {
  50: Color(0xFF1DA1F2),
  100: Color(0xFF1DA1F2),
  200: Color(0xFF1DA1F2),
  300: Color(0xFF1DA1F2),
  400: Color(0xFF1DA1F2),
  500: Color(0xFF1DA1F2),
  600: Color(0xFF1DA1F2),
  700: Color(0xFF1DA1F2),
  900: Color(0xFF1DA1F2),
});

extension ColorExt on String? {
  Color? toColor() {
    final hexString = this;
    if (hexString != null) {
      final buffer = StringBuffer();
      if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
      buffer.write(hexString.replaceFirst('#', ''));
      return Color(int.parse(buffer.toString(), radix: 16));
    }
    return null;
  }
}
