import 'dart:io';

import 'package:ashvin_jadav/utils/strings.dart';

class AppConstant {}

getFont() {
  return 'Roboto';
}

List<String> ROLE_LIST = [
  PRODUCT_DESIGNER,
  FLUTTER_DEVELOPER,
  QA_TESTER,
  PRODUCT_OWNER
];
